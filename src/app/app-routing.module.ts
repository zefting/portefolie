import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Pages
import { PortefolieComponent } from './portefolie/portefolie.component';
import { PortefoliedetailComponent } from './portefoliedetail/portefoliedetail.component';

const routes: Routes = [
  { path: '', redirectTo: '/portefolie', pathMatch: 'full' },
  { path: 'portefolie', component: PortefolieComponent },
  { path: 'portdetail', component:PortefoliedetailComponent}

  //{ path: '', component: Component  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
