import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortefolieComponent } from './portefolie.component';

describe('PortefolieComponent', () => {
  let component: PortefolieComponent;
  let fixture: ComponentFixture<PortefolieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortefolieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortefolieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
