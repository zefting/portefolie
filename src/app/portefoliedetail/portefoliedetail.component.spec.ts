import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortefoliedetailComponent } from './portefoliedetail.component';

describe('PortefoliedetailComponent', () => {
  let component: PortefoliedetailComponent;
  let fixture: ComponentFixture<PortefoliedetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortefoliedetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortefoliedetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
