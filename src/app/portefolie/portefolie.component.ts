import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PortefoliedetailComponent } from '../portefoliedetail/portefoliedetail.component';
import cards from './../../assets/cards.json'

@Component({
  selector: 'app-portefolie',
  templateUrl: './portefolie.component.html',
  styleUrls: ['./portefolie.component.css']
})
export class PortefolieComponent implements OnInit {

  constructor(private modalService: NgbModal) { }
  @Output() viewDetails = new EventEmitter<any>();
  public cardsList: [] = cards;
  open(card) {
    const modalRef = this.modalService.open(PortefoliedetailComponent);
    modalRef.componentInstance.cardinfo = card;
  }


  /*[
    {
      title: 'ERP project - Angular, Bootstrap and C#',
      imgsource: './assets/thumbnails/ERPfront.png',
      imgtext:
      [
        {desc:'Angular'}, {desc:'Bootstrap 4'}, {desc:'C#'}, {desc:'MySQL'}, {desc:'ASP.net'}, {desc:'REST API'},
      ],
      url:'/Nicolaj/erpc',
      description1: 'This project was designed to serve a dual purpose:',
  list: [
    {listDescription: 'Setting distinations, picking a boat, a cox (if the boat requires a cox) and rowers in a dynamic way'},
    {listDescription: 'Memberservice module where both infomations on the members can be viewed and edited, and steps in the rowers education in the club can be tracked'}
        ],
  description2: 'The project considst of a front-end powered by Angular 9, Bootstrap 4 and Ng-Bootstrap and a back-end in Dotnet Core and MySql',
  Bitbucket: [
    {bitbucketDescription: "Code repository for the project's front-end",
    link: 'https://bitbucket.org/zefting/erp/src/master/DotnetCore%20version%20%2B%20Angular%20frontend/front/'},
    {bitbucketDescription: "Code repository for the project's back-end",
    link: 'https://bitbucket.org/zefting/erp/src/master/DotnetCore%20version%20%2B%20Angular%20frontend/ERP/'}
  ],
  liveViewLink: '',
  list2:[]
      },

    {
      title: 'ERP project - Angular, Bootstrap and PHP',
      url:'/Nicolaj/erpphp',
      imgsource: './assets/thumbnails/ERPfront.png',
      imgtext:
      [
        {desc:'Angular'}, {desc:'Bootstrap 4'}, {desc:'PHP'}, {desc:'MySQL'}, {desc:'Lumen'}, {desc:'Laravel'}
      ],
      description1: 'This project was designed to be a more slim and lightweight version of the ERP project - Angular, Bootstrap and C# project. The project features a Memberservice module where both infomations on the members can be viewed and edited, and steps in the rowers education in the club can be tracked',
  list: [],
  description2: 'The project considst on a front-end powered by Angular 9, Bootstrap 4 and Ng-Bootstrap and a back-end in the lightweight micro-framwork Lumen and MySql',
  Bitbucket: [
    {bitbucketDescription: "Code repository for the project's front-end",
    link: 'https://bitbucket.org/zefting/erp/src/master/PHP%20version%20%2B%20Angular%20frontend/lumen/lumenfront/'},
    {bitbucketDescription: "Code repository for the project's back-end",
    link: 'https://bitbucket.org/zefting/erp/src/master/PHP%20version%20%2B%20Angular%20frontend/ERPLumen/'}
  ],
  liveViewLink: '',
  list2:[]
    }
    ,{
      title: 'Progressiv Web App Example 1',
      imgsource: './assets/thumbnails/pwa2_thumb.png',
      imgtext:
      [
        {desc:'Angular'}, {desc:'Progressiv Web App'}, {desc:'Responsiv'}, {desc:'HTML 5/CSS'}
      ],
      url:'/Nicolaj/pwa2',
      description1: 'This project is an example on a mobile-first travel website build as a progressiv web app. The focus in this example was to build an app that:',
      list: [
        {listDescription: 'Handles booking through two modal services and dynamic page loading (see the booking page)'},
        {listDescription: 'Impliments full PWA standards'}
        ],
      description2: 'On this site there is a mock booking system where you can select a travel destintion and hotel. The data is not transmittet anywhere. The app can be downloaded as an app and the user will be prompted to do so. The project is develuped with Angular 9, Bootstrap 4 and ngx-bootstrap',
      Bitbucket: [
        {bitbucketDescription: 'Code repository:',
        link: 'https://bitbucket.org/zefting/travel-booking-app/src/master/'},
      ],
      liveViewLink: 'https://zefting.dk/pwa2',
  list2:[
    {listopen:'The app lands on a large image with bright colors:' ,list2Description: ' <br>\r\n There is only a warning about this being a mock-site and a notice about the option to download the app on the frontpage. <br>\r\n The reson behind this is, that the focus for this project is not UX design, but the things listed above.\r\n<br>', img2:'./assets/pwa2/front.png'},
    {listopen:'' ,list2Description: '<b>The booking page</b> is the most interesting about this example and was designed mobile fist. <br> The minimum working size for the design is 414 X 736 pixel <br> Landing image below: <br>', img2:'./assets/pwa2/booking_landing_page.png'},
    {listopen:'' ,list2Description: 'On this page the user selects a travel destination by clicking on one of three image inputs.', img2:'./assets/pwa2/img_input.png'},
    {listopen:'' ,list2Description: 'On destination select, a dropdown appears where the user can select a city to travel to', img2:'./assets/pwa2/citySelect.png'},
    {listopen:'' ,list2Description: 'On destination select, a dropdown appears where the user can select a city to travel to', img2:'./assets/pwa2/input.png'},
    {listopen:'' ,list2Description: 'A table with bookable hotels a peers filtered by city, and the user can select a hotel to stay at.', img2:'./assets/pwa2/hotels.png'},
    {listopen:'' ,list2Description: 'The user can then select a room at the selected hotel.', img2:'./assets/pwa2/rooms.png'},
    {listopen:'' ,list2Description: 'After selecting a room, the user is send to a confermation site to validate the order. <br>\r\n No payment service have been inplimentet for this example. It is also posiable to pass the order information to an API from one of the major travel sites.', img2:'./assets/pwa2/roomSelected.png'}
    ]

    },
    {
      title: 'Aarhus Studenter Roklub',
      imgsource: './assets/thumbnails/asrdk.png',
      imgtext:
      [
        {desc:'Wordpress'}, {desc:'PHP'}, {desc:'MySQL'}, {desc:'C#'}, {desc:'Email Automation'}
      ],
      url: '/Nicolaj/asrdk',
      description1: 'This website is developed and maintained in collaboration with others. The site serves a the main communication platform for the clubs members. A part of this communication flow is a newsletter.',
      list: [],
      description2: 'The page is using Wordpress and a C#-based newsletter sender',
      Bitbucket: [
      {bitbucketDescription: '',
      link: ''},
      ],
      liveViewLink: 'https://www.asr.dk',
      list2:[]
    },
    {
      title: 'Portefolie',
      imgsource: './assets/thumbnails/portefolie.png',
      imgtext:
      [
        {desc:'Angular'}, {desc:'HTML/CSS'}, {desc:'Bootstrap 4'}
      ],
      url: '/Nicolaj/portefolieinfo',
      description1: "This is the code that runs this subpage on this website. It is build in Angular 9 and Bootstrap 4. The portefolie page generates each card dynamically based on a local json array in the page's component. The CV-page is a build as a static page.",
  list: [],
  description2: '',
  Bitbucket: [
    {bitbucketDescription: 'Code repository for this portefolie',
    link: 'https://bitbucket.org/zefting/portefolie/src/master/'},
  ],
  liveViewLink: 'www.zefting.dk/Nicolaj',
  list2:[]
    },
    {
      title: 'Card grid based selector',
      imgsource: './assets/thumbnails/grid-select.png',
      imgtext:
      [
        {desc:'Angular'}, {desc:'HTML/CSS'}, {desc:'Bootstrap 4'}
      ],
      url: '/Nicolaj/cardgrid',
      description1: 'This project provides an example on store exploring a design with cards placed in a Bootstrap 4 grid as a navigation tool. The first page is kept simple but with warm colors provided by the images. This project is only on a limited basis mobile friendly.',
      list: [],
      description2: 'Each card linkes to a new page that is intended as a product page, but for this project there has not been implimented a store-system. The project is develuped with Angular 9, Bootstrap 4 and ngx-bootstrap',
      Bitbucket: [
        {bitbucketDescription: 'Code repository for the Card Grid Selector',
        link: 'https://bitbucket.org/zefting/grid-select/src/master/'},
      ],
      liveViewLink: 'http://zefting.dk/Grid-Select/',
      list2:[]
    },

    {
      title: 'Owl-Slider Based Shop',
      imgsource: './assets/thumbnails/Owl-Slider-Based-Shop.png',
      imgtext:
      [
        {desc:'Angular'}, {desc:'HTML/CSS'}, {desc:'Bootstrap 4'}
      ],
      url:'/Nicolaj/owlshop',
      description1: 'This project provides an example on store providing courses in the medical field. The booking page does not do anything in the example, but a rest-api could be added sending the booking information to a booking platform, or a system that after validation sends an email with the booking information to the owner/ teacher. "Our Work" page a card-based carousel with customer references has been implimented based on the angular directive Owl-slider.',
      list: [],
      description2: 'The project is develuped with Angular 9, Bootstrap 4 and ngx-bootstrap',
      Bitbucket: [
        {bitbucketDescription: 'Code repository for Grid selecter',
        link: 'https://bitbucket.org/zefting/grid-select/src/master/'},
      ],
      liveViewLink: 'http://zefting.dk/Owl-Slider-Based-Shop/home',
      list2:[]
    },

    {
      title: 'Card tile slider',
      imgsource: './assets/thumbnails/card-tile-selector.png',
      imgtext:
      [
        {desc:'Angular'}, {desc:'HTML'}, {desc:'Bootstrap 4'}
      ],
      url:'/Nicolaj/cardtile',
  description1: 'This project provides an example on store or blog exploring a design with a card-carousel as a navigation tool. Each card is clickable. A major focus point in has been to mix video and images in the cards headers. This project is only on a limited basis mobile friendly.',
  list: [],
  description2: 'Each card linkes to a new page that is intended as a product page, but for this project there has not been implimented a store-system. The project is develuped with Angular 9, Bootstrap 4 and ngx-bootstrap',
  Bitbucket: [
    {bitbucketDescription: 'Code repository for Grid-tile selecter',
    link: 'https://bitbucket.org/zefting/card-tile/src'},
  ],
  liveViewLink: 'http://zefting.dk/Card-Tile-Slider/',
  list2:[]
    },
    {
      title: 'FileServerUploadExample',
      url:'/Nicolaj/file1',
      imgsource: './assets/thumbnails/file1.png',
      imgtext:
        [
          {desc:'C#'}, {desc:'File upload'}, {desc:'ASP.net'}, {desc:'REST API'},
        ],
        description1: 'This is a file upload example programmed with a simple angular 9 front-end and .Net Core backend. The file is uploaded to the server, renamed and saved in a folder.',
        list: [],
        description2: '',
        Bitbucket: [
          {bitbucketDescription: 'Code repository for the FileUpload-project',
          link: 'https://bitbucket.org/zefting/fileupload/src/master/'},
        ],
        liveViewLink: '',
        list2:[]
    },

    {
      title: 'Beer store',
      url:'/Nicolaj/beer',
      imgsource: './assets/thumbnails/file1.png',
      imgtext:
        [
          {desc:'C#'}, {desc:'File upload'}, {desc:'ASP.net'}, {desc:'REST API'},
        ],
        description1: '',
        list: [],
        description2: '',
        Bitbucket: [
        {bitbucketDescription: 'Code repository',
        link: ''},
        ],
        liveViewLink: '',
  list2:[]
    },
    {
      title: 'Raspberry Pi Underwater camera in Python',
      imgsource: './assets/thumbnails/Underwatercam.jpeg',
      imgtext:
      [
        {desc:'Python'}, {desc:'Underwater'}, {desc:'Astral'}, {desc:'Image processing'}, {desc:'Raspberry PI'}
      ],
      url:'/Nicolaj/uw',
      description1: 'This project is an underwater camera to monitor the migration of spawning Sea trouts in a steam.',
  list: [],
  description2: 'The project runs on a Raspberry Pi with camera equipped a motorized night-vison lense. The project utilizes verious Python libraries to control the camera and log the frases of the moon, tide and relative position of the sun when the picture is taken',
  Bitbucket: [
    {bitbucketDescription: 'Code repository for the camera-project',
    link: 'https://bitbucket.org/zefting/fishcam/src/master/'},
  ],
  liveViewLink: '',
  list2:[]
    },

    {title: 'FileServerUploadExample2',
    imgsource: './assets/thumbnails/FileUploadExample2.png',
    imgtext:
    [
      {desc:'C#'}, {desc:'ASP.Net'}, {desc:'MySql'}, {desc:'Filename'}, {desc:'Server'}
    ],
    url:'/Nicolaj/file2',
    description1: 'This is a file upload example programmed with a .Net Core backend. The file is uploaded to the server, sorted into one of to folders (jpg and pgn) and then renamed with a GUID + filename. The old and new filename is stored into a MySql database that is used to retrive the file and serve it back to the user with the original filename. For this example no user mangement system have been implimented.',
    list: [],
    description2: 'This example can take a renamed file input and retrive that file, rename it to the original filename, and then return that file to the user',
    Bitbucket: [
      {bitbucketDescription: 'Code repository for the FileUpload-project',
      link: 'https://bitbucket.org/zefting/fileuploadexample2/src/master/'},
    ],
    liveViewLink: '',
    list2:[]
  },

  {
    title: 'ChartExample1',
    imgsource: './assets/thumbnails/chartexample1.png',
    imgtext:
    [
      {desc:'Angular'}, {desc:'Charts'}, {desc:'Dataprocessing'}, {desc:'HTML 5/CSS'}
    ],
    url:'/Nicolaj/chartexample1',
    description1: 'This is a small example site working with various charts and data handling. No backend have been implimented for this project, however the script is build with this in mind.',
    list: [],
    description2: 'A list over fictive products sold have been programmed. This works with local json data.',
    Bitbucket: [
      {bitbucketDescription: 'Code repository for the Chartexample1-project',
      link: 'https://bitbucket.org/zefting/chartexample1/src/master/'},
    ],
    liveViewLink: 'http://zefting.dk/ChartExample1/',
    list2:[]
  },

  {
    title: 'angular-front1',
    imgsource: './assets/thumbnails/angular-front1.png',
    imgtext:
    [
      {desc:'Angular'}, {desc:'Charts'}, {desc:'Responsiv'}, {desc:'HTML 5/CSS'}
    ],
    url:'/Nicolaj/angular-front1',
    description1: 'This project is an example on a mobile-first travel website. The focus in this project was to build a responsiv site with responsive images and overlays.',
  list: [],
  description2: 'On this site there is a mock booking system where you can select a travel destintion, airport and hotel. The data is not transmittet anywhere. The project is develuped with Angular 9, Bootstrap 4 and ngx-bootstrap',
  Bitbucket: [
    {bitbucketDescription: 'Code repository for the Angular-Front1',
    link: 'https://bitbucket.org/zefting/angular-front1/src/master/'},
  ],
  liveViewLink: 'http://zefting.dk/angular-front1/',
  list2:[]
  }
  ];
  */
  ngOnInit(): void {

    console.log(this.cardsList);
  }
  Clickcard(card){
    console.log('clicked',card);
    this.viewDetails.emit(card);

  }



}
